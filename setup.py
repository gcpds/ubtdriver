import os
from setuptools import setup, find_packages

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='ubtdriver',
    version='1.1',
    packages=find_packages(),

    author='Yeison Cardona',
    author_email='yencardonaal@unal.edu.co',
    maintainer='Yeison Cardona',
    maintainer_email='yencardonaal@unal.edu.co',

    # url = '',
    download_url='https://bitbucket.org/gcpds/ubtlauncher',

    install_requires=['pycollect',
                      'tornado',
                      'scipy',
                      ],

    include_package_data=True,
    license='BSD License',
    description="",

    classifiers=[
        'Intended Audience :: Healthcare Industry',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Topic :: Scientific/Engineering :: Medical Science Apps',
        'Topic :: Scientific/Engineering :: Visualization',
    ],

    entry_points={
        'console_scripts': ['ubtdriver=ubtdriver.__main__:main'],
    }

)
