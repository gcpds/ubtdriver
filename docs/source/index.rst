.. UBT Driver documentation master file, created by
   sphinx-quickstart on Thu Jul  5 10:47:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

UBT Driver
============

Desktop driver for integration with `UBT application <http://>`_.
This program serve a websocket, handle connections with serial devices and
collect data from them.

Supported devices:

  * CARESCAPE Monitor B650
  * CARESCAPE Monitor B450


.. image:: _static/screenshots/main.png
   :align: center


Navigation
----------

.. toctree::
   :maxdepth: 2
   :name: mastertoc

   content/getting_started
   content/modes


Live protocols
--------------

.. toctree::
   :maxdepth: 2
   :name: mastertoc

   content/live_protocols
   content/protocols/pycollect






Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
