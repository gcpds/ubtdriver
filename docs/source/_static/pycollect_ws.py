from browser import alert, document, window
from radiant import WebSocket

from mdcframework.mdc import MDCButton, MDCFormField
import os
import json

########################################################################
class PyCollectWS(WebSocket):
    """"""

    #----------------------------------------------------------------------
    def on_open(self, *args, **kwargs):
        """"""
        print('Websocket open!!')
        document.select('#connect-status')[0].html = 'CONNECTED'

        for button in document.select('.mdc-button'):
            setattr(button, 'disabled', False)

        b = document.select('.mdc-button#open_socket')[0]
        setattr(b, 'disabled', True)


        self.list_waveforms_checkbox = []
        self.list_subrecords_checkbox = []
        self.list_labels_checkbox = []

        self.update_request_subrecord()
        self.update_request_waveform()
        self.update_request_labels()

        EDFHeader.update_edf_header()


    #----------------------------------------------------------------------
    def on_close(self, *args, **kwargs):
        """"""
        print('Websocket closed!!')
        document.select('#connect-status')[0].html = 'CLOSED\nUBT Launcher is running?'



        for button in document.select('.mdc-button'):
            setattr(button, 'disabled', True)

        b = document.select('.mdc-button#open_socket')[0]
        setattr(b, 'disabled', False)



    #----------------------------------------------------------------------
    def on_message(cls, evt):
        """"""
        status_name = 'debug'
        data = json.loads(evt.data)

        print("Message received: {}".format(data))

        out = document.select('#{command}'.format(**data[status_name]))



        if out:
            if data[status_name]['command'] == 'request_waveforms':
                out[0].text += "\n\n" + json.dumps(data)
            else:
                out[0].text = json.dumps(data)


        if data[status_name]['command'] == 'available_subrecords':
            cls.update_list_subrecords(data['subrecords'])

        elif data[status_name]['command'] == 'possible_waveforms':
            cls.update_list_waveforms(data['waveforms'])

        elif data[status_name]['command'] == 'available_channels':
            cls.update_list_labels(data['channels'])

        elif data[status_name]['command'] == 'get_csv':

            placeholder = document.select('#csv_download_placeholder')[0]

            for filename in data['filenames']:
                path = filename
                name = os.path.split(filename)[-1]
                button = MDCButton('{}'.format(name), href=path, disabled=True, raised=False, style={'margin-bottom': '30px', 'margin-right': '15px', 'text-transform': None}, attr={'download':name})
                placeholder <= button


        elif data[status_name]['command'] == 'get_raw':

            placeholder = document.select('#raw_download_placeholder')[0]

            for filename in data['filenames']:
                path = filename
                name = os.path.split(filename)[-1]
                button = MDCButton('{}'.format(name), href=path, disabled=True, raised=False, style={'margin-bottom': '30px', 'margin-right': '15px', 'text-transform': None}, attr={'download':name})
                placeholder <= button





        elif data[status_name]['command'] == 'get_edf':

            placeholder = document.select('#edf_download_placeholder')[0]

            for filename in data['filenames']:
                path = filename
                name = os.path.split(filename)[-1]
                button = MDCButton('{}'.format(name), href=path, disabled=True, raised=False, style={'margin-bottom': '30px', 'margin-right': '15px', 'text-transform': None}, attr={'download':name})
                placeholder <= button




    #----------------------------------------------------------------------
    def update_list_subrecords(self, subrecords):
        """"""

        self.list_subrecords_checkbox = []
        window.list_subrecords.clear()

        for subrecord in subrecords:

            # from mdcframework.mdc import MDCButton, MDCFormField, MDCForm

            ff2 = MDCFormField()
            ff2.style = {
                        'width': '250px',
                        'display': 'inline-block',
                        "line-height": '2.25rem',
                        }

            chbox = window.list_subrecords.mdc.Checkbox(subrecord, value=subrecord, formfield=ff2)
            chbox.bind('click', self.update_request_subrecord)
            self.list_subrecords_checkbox.append(chbox)



    #----------------------------------------------------------------------
    def update_list_waveforms(self, waveforms):
        """"""
        self.list_waveforms_checkbox = []
        window.list_waveforms.clear()

        for wave in waveforms:

            ff2 = MDCFormField()
            ff2.style = {
                        'width': '250px',
                        'display': 'inline-block',
                        "line-height": '2.25rem',
                        }

            chbox = window.list_waveforms.mdc.Checkbox(wave, value=wave, formfield=ff2)
            chbox.bind('click', self.update_request_waveform)
            self.list_waveforms_checkbox.append(chbox)


    #----------------------------------------------------------------------
    def update_list_labels(self, labels):
        """"""
        self.list_labels_checkbox = []
        window.list_labels.clear()

        for wave in labels:

            ff2 = MDCFormField()
            ff2.style = {
                        'width': '250px',
                        'display': 'inline-block',
                        "line-height": '2.25rem',
                        }

            chbox = window.list_labels.mdc.Checkbox(wave, value=wave, formfield=ff2)
            chbox.bind('click', self.update_request_labels)
            self.list_labels_checkbox.append(chbox)



    #----------------------------------------------------------------------
    def update_request_subrecord(self, event=None):
        """"""
        subs = []
        for chbox in self.list_subrecords_checkbox:
            if chbox.mdc.checked:
                subs.append(chbox.mdc.value)

        window.request_subrecords = {"command": "request_subrecords",
                                     'subrecords': subs,}
        out = document.select('#available_subrecords_selected')[0]
        out.text = json.dumps(window.request_subrecords)




    #----------------------------------------------------------------------
    def update_request_waveform(self, event=None):
        """"""
        subs = []
        for chbox in self.list_waveforms_checkbox:
            if chbox.mdc.checked:
                subs.append(chbox.mdc.value)

        window.request_waveforms = {"command": "request_waveforms",
                                    'waveforms': subs,}
        out = document.select('#available_waveforms_selected')[0]

        out.text = json.dumps(window.request_waveforms)


    #----------------------------------------------------------------------
    def update_request_labels(self, event=None):
        """"""
        subs = []
        for chbox in self.list_labels_checkbox:
            if chbox.mdc.checked:
                subs.append(chbox.mdc.value)

        window.request_labels = {"command": "request_channels",
                                 'channels': subs,}
        out = document.select('#available_labels_selected')[0]

        out.text = json.dumps(window.request_labels)





########################################################################
class EDFHeader:
    """"""


    #----------------------------------------------------------------------
    @classmethod
    def update_edf_header(self, event=None):
        """"""

        b = document.select('#edf_birthdate input')[0].value
        if not b:
            b = '0'

        g = document.select('#edf_gender select')[0].value
        if not g:
            g = '0'

        data = {
        "adminCode": document.select('#edf_admincode input')[0].value,
        "birthDate": int(b),
        "equipment": document.select('#edf_equipment input')[0].value,
        "gender": int(g),
        "patientCode": document.select('#edf_patientcode input')[0].value,
        "patientName": document.select('#edf_patientname input')[0].value,
        "patientAdditional": document.select('#edf_patientaditional input')[0].value,
        "recordingAdditional": document.select('#edf_recordingaditional input')[0].value,
        "technician": document.select('#edf_technician input')[0].value,
        }

        request = {'command': 'get_edf',
                   'patientInfo': data,
                   'annotations': {'size': 2,
                                   'items': [
                                       {'onset': 779.94,
                                        'duration': 0.004,
                                        'description': 'OJOS_CERRADOS',
                                        },

                                       {'onset': 899.92,
                                        'duration': 0.004,
                                        'description': '2_MIN',
                                        },
                                   ],

                                   },
                   }

        out = document.select('#request_edf_header')[0]
        out.text = json.dumps(request)
        window.request_get_edf = request











