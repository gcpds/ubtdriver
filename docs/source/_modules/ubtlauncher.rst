ubtlauncher package
===================

.. automodule:: ubtlauncher
    :members:
    :no-undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    ubtlauncher.childs
    ubtlauncher.handlers.old
    ubtlauncher.wsserver

Submodules
----------

.. toctree::

   ubtlauncher.frame
   ubtlauncher.methods
   ubtlauncher.popup

