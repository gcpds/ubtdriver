===============
Getting started
===============

Install
=======

From source
-----------

.. code:: bash

   git clone https://yeisoneng@bitbucket.org/gcpds/pycollect.git
   cd pycollect
   python setup.py install

   cd ..

   git clone https://yeisoneng@bitbucket.org/gcpds/ubtdriver.git
   cd ubtdriver
   python setup.py install



From PyPi (not available yet)
-----------------------------

.. code:: bash

   pip install ubtdriver



Install dependencies
--------------------

.. code:: bash

   pip install tornado pyserial pyedflib pandas>=0.23.1


Run
===

The device must be connected before to run ``ubtdriver``.

.. code:: bash

   python -m ubtdriver

or

.. code:: bash

   ubtdriver
