  * ``available_channels``: Request the list of available subrecords based on the monitor modules activity merged with the list of waveforms supported by the monitor.
  * ``available_subrecords``: Request the list of available subrecords based on the monitor modules activity.
  * ``clear_data``: Clear buffer and stop data transfer.
  * ``close_stream``: Stop stream and close serial port, save data if a recording enabled.
  * ``debug``: Request debug information about device, conection, data transfering.
  * ``get_csv``: Generate and save data un CSV format.
  * ``get_edf``: Generate and save data in EDF+ format.
  * ``get_raw``: Generate and save data in RAW format.
  * ``list_all_measures``: Request a list of all measures supported by the monitor.
  * ``list_all_waveforms``: Request a list of all waveform supported by the monitor.
  * ``possible_waveforms``: Request the list of waveforms supported by the monitor.
  * ``reconnect_device``: Send an instruction for reconnect with the serial port.
  * ``request_channels``: Request to monitor a set of waveforms and subrecords.
  * ``request_subrecords``: Request to monitor a set of subrecords.
  * ``request_waveforms``: Request to monitor a set of waveforms.
  * ``start_recording``: Clear data, continues the data transfer.
  * ``stop_recording``: 
  * ``stop_transfer``: Stop the monitor data transfer.
