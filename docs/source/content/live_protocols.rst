==============
Live protocols
==============

Live protocols are a web implementation and explanation about how to connect with
de desired device, either in :ref:`Debug mode` or :ref:`Normal mode`.

For devices like `CARESCAPE Monitor B650` or `CARESCAPE Monitor B450` the
protocol are explained in :ref:`PyCollect - Live protocol`.

For devices like `MASIMO` or `CARESCAPE Monitor B450` the protocol are
explained in :ref:`PyMasimo - Live protocol`.



Troubleshooting
===============


``DOMException: "The operation is insecure."``
----------------------------------------------

This could happend in *Firefox*, to fix it just got to `about:config <about:config>`_
and and toggle the option ``network.websocket.allowInsecureFromHTTPS``.