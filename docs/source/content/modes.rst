=====
Modes
=====


Debug mode
----------

The debug mode can be enable by check in ``Debug mode`` option in the main window.

.. image:: ../_static/screenshots/debug_mode.png
    :align: center

This option will load `fake data` from a local database, but will serve it as
original data read from monitor, `debug mode` also will debug serial communication,
this means that an optional serial device can be connected and the system will
response to their availability.



Normal mode
-----------

In this mode the system will attempt to connect with a real monitor.

.. image:: ../_static/screenshots/normal_mode.png
    :align: center
