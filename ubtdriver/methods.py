import os
import sys
import time
import signal
from pathlib import Path
from tkinter import NORMAL

import serial

from ubtdriver.wsdriver import run_websocket, stop_websocket


########################################################################
class Methods:
    """Methods inherited by the main window."""

    #----------------------------------------------------------------------
    def get_config(self):
        """Generate configuration for WebSocket."""

        user_dir = os.path.join(Path.home(), 'ubtdriver')

        config = {
            'device': self.MODULES_HANDLER[self.module.get()],
            'port': self.selected_port,
            'ip_port': self.ip_port.get(),
            'debug_mode': self.debug_mode.get(),
            'user_dir': user_dir
            }

        if not os.path.exists(user_dir):
            os.makedirs(user_dir, exist_ok=True)

        return config


    #----------------------------------------------------------------------
    def run_server(self):
        """Run WebSocket in background."""

        config = self.get_config()
        self.server = run_websocket(config, self.logger)

        self.output.show()
        self.button_start_server.hide()
        self.button_stop_server.show()


    #----------------------------------------------------------------------
    def stop_server(self):
        """Stop tornado server."""

        if hasattr(self, 'server'):
            self.server.stop()

        stop_websocket()

        self.button_start_server.configure(state=NORMAL)

        self.button_start_server.show()
        self.button_stop_server.hide()


    #----------------------------------------------------------------------
    def close_frame(self, *args, **kwargs):
        """Destroy window."""

        try:
            self.stop_server()
        except:
            pass

        try:
            stop_websocket()
        except:
            pass

        self.logger.put("<EXIT>")
        self.destroy()

        #This must kill the window and the threads with their servers.
        try:
            os.kill(os.getpid(), signal.SIGUSR1)
        except:
            pass


    #----------------------------------------------------------------------
    def update_launch_button(self, *args, **kwargs):
        """Launch button only will be available when all settings are completed."""

        if self.module.get() != self.DEAFULT_TEXT_DEVICE and (self.port.get() != self.DEAFULT_TEXT_PORT or self.debug_mode.get()):
            self.button_start_server.configure(state=NORMAL)


    #----------------------------------------------------------------------
    def set_port(self, port):
        """Select port.

        Parameters
        ----------
        port : str
            The serial port name.
        """

        self.selected_port = port
        self.update_launch_button()


    #----------------------------------------------------------------------
    def list_available_ports(self):
        """Scan available ports.

        Returns
        -------
        list
            Array of available serial ports.
        """

        osname = os.name

        if osname == 'nt':
            prefix = 'COM{}',
        elif osname == 'posix':
            prefix = '/dev/ttyACM{}', '/dev/ttyUSB{}',

        list_ports = []

        for pref in prefix:
            for i in range(20):
                port = pref.format(i)
                try:
                    d = serial.Serial(port, timeout=0.1)
                    d.close()
                    list_ports.append(port)
                except:
                    continue

        return list_ports
