import os
from tkinter import FLAT, DISABLED, N, S, W, E
from tkinter import Frame, StringVar, OptionMenu, Button, Text, Entry, Label, Checkbutton, BooleanVar, LabelFrame, Image

from ubtdriver.methods import Methods
from ubtdriver.tools import CustomPopup, WidgetLogger

from pathlib import Path


########################################################################
class UBTDriver(Frame, Methods):
    """TkInter main window."""

    DEAFULT_TEXT_DEVICE = 'Select device'
    DEAFULT_TEXT_PORT = 'Select serial port'
    DEFAULT_IP_PORT = '8890'
    DOC_URL = 'https://ubt-launcher.readthedocs.io/en/latest/'

    MODULES_HANDLER = {
        'Masimo': 'masimo',
        'CARESCAPE Monitor B650/B450': 'gehealthcare',
    }


    #----------------------------------------------------------------------
    def __init__(self, master):
        """Main Frame.

        Parameters
        ----------
        master: Tk object
            Tk root.
        """

        Frame.__init__(self, master)

        self.parent = master

        W_, H_ = 720, 170
        X_, Y_ = (self.parent.winfo_screenwidth() - W_) // 2, (self.parent.winfo_screenheight() - H_) // 6
        self.parent.geometry("{}x{}+{}+{}".format(W_, H_, X_, Y_))


        # self.parent.resizable(True, True)
        self.parent.title('UBT Driver')
        self.parent.config(padx=10, pady=10)

        self.parent.grid_columnconfigure(0, weight=1)
        # self.parent.grid_columnconfigure(1, weight=1)

        self.parent.grid_rowconfigure(3, weight=1)

        self.parent.bind("<Destroy>", self.close_frame)

        self.selected_port = None

        self.icon = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'assets')
        if not os.path.exists(self.icon):
            self.icon = os.path.join(Path.home(), 'ubtdriver', 'ubtdriver', 'assets')

        if os.path.exists(self.icon):
            if os.name == "nt":
                self.parent.iconbitmap(bitmap=os.path.join(self.icon, 'icon.ico'))
            else:
                img = Image("photo", file=os.path.join(self.icon, 'icon.gif'))
                self.parent.call('wm', 'iconphoto', self.parent._w, img)

        self.module = StringVar(self)
        self.module.set(self.DEAFULT_TEXT_DEVICE)

        self.port = StringVar(self)
        self.port.set(self.DEAFULT_TEXT_PORT)

        self.ip_port = StringVar(self)
        self.ip_port.set(self.DEFAULT_IP_PORT)

        self.debug_mode = BooleanVar(self)
        self.debug_mode.set(False)

        # Frame device
        frame_device = LabelFrame(self.parent, text='Device', height=18, padx=5, pady=5)
        frame_device.grid(row=0, column=0, sticky=N+S+W+E, pady=(0, 5))
        self.build_frame_device(frame_device)

        # Frame broadcast
        frame_brodcast = LabelFrame(self.parent, text='Broadcast', height=18, padx=5, pady=5)
        frame_brodcast.grid(row=2, column=0, sticky=N+S+W+E)
        self.build_frame_brocast(frame_brodcast)

        # Debugger
        self.output = Text(self.parent, bg="#444444", fg="#ffffff", height=10, borderwidth=5, relief=FLAT, highlightthickness=0, font="mono 10", wrap="none")
        self.output.show = self.show_output
        self.output.config(state=DISABLED)
        self.output.hide = self.output.grid_forget

        self.logger =  WidgetLogger(self.output).result_queue


    #----------------------------------------------------------------------
    def show_output(self):
        """Show debug frame."""

        if not self.output.winfo_ismapped():
            self.output.grid(row=3, column=0, sticky=W+E+S+N, padx=(0, 0), pady=(5, 0))
            self.parent.geometry("{}x{}".format(self.parent.winfo_width(), self.parent.winfo_height() + 200))


    #----------------------------------------------------------------------
    def build_frame_device(self, parent):
        """Generate and put th elements from `Device` LabelFrame."""

        parent.grid_columnconfigure(0, weight=1)

        self.listbox = OptionMenu(parent, self.module, *self.MODULES_HANDLER.keys(), command=self.update_launch_button)
        self.listbox.grid(row=0, column=0, sticky=N+S+W+E)

        list_ports = self.list_available_ports()

        if list_ports:
            self.listbox = OptionMenu(parent, self.port, command=self.set_port, *list_ports)
        else:
            self.listbox = OptionMenu(parent, self.port, command=self.set_port, value=None)
        self.listbox.grid(row=0, column=1, sticky=N+S+W+E)

        debug_checkbutton = Checkbutton(parent, text="Debug mode", variable=self.debug_mode, command=self.update_launch_button)
        debug_checkbutton.grid(row=1, column=0, sticky=W)
        CustomPopup(debug_checkbutton, 'It does not connect with the monitor, simulate transfer from a preexisting raw data.', command=None)

        frame_brodcast = LabelFrame(self.parent, text='Broadcast', height=18, padx=5, pady=5)
        frame_brodcast.grid(row=2, column=0, sticky=N+S+W+E)


    #----------------------------------------------------------------------
    def build_frame_brocast(self, parent):
        """Generate and put th elements from `Broadcast` LabelFrame."""

        parent.grid_columnconfigure(1, weight=1)
        parent.grid_rowconfigure(1, weight=1)
        parent.configure(padx=5, pady=5)

        label = Label(parent, text='Port: ')
        label.grid(row=0, column=0, sticky=W)

        input_port = Entry(parent, textvariable=self.ip_port)
        input_port.grid(row=0, column=1, sticky=W)

        self.button_start_server = Button(parent, text='Run server', command=self.run_server, state=DISABLED)
        self.button_start_server.grid(row=0, column=2, sticky=E)
        self.button_start_server.hide = self.button_start_server.grid_forget
        self.button_start_server.show = lambda : self.button_start_server.grid(row=0, column=2, sticky=E)

        self.button_stop_server = Button(parent, text='Stop server', command=self.stop_server)
        self.button_stop_server.hide = self.button_stop_server.grid_forget
        self.button_stop_server.show = lambda : self.button_stop_server.grid(row=0, column=2, sticky=E)
