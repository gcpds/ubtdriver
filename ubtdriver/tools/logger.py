"""
======
Logger
======

This logger feature can write message asynchronously just changing the value of
a ``Queue`` object.
"""

import time
from threading import Thread
from datetime import datetime
from queue import Queue, Empty
from tkinter import NORMAL, DISABLED, END


########################################################################
class WidgetLogger:
    """Asynchronous logger."""

    #----------------------------------------------------------------------
    def __init__(self, widget):
        """Connect the widget with the queue processor.

        Parameters
        ==========

        widget: Text (TkInter)
            Logs will be writed in this widget.
        """

        self.widget = widget
        self.result_queue = Queue()
        Thread(target=self.update_log, args=(self.result_queue, )).start()


    #----------------------------------------------------------------------
    def update_log(self, q):
        """Thread that check preridically the queue content.

        Parameters
        ==========
        q: Queue object
            The content of this queue will be writed to Text widget and cleaned after.
        """

        while True:

            try:
                log = q.get(block=False)

                if log == '<EXIT>':  #window closed
                    return

                # if '<MESSAGEBOX>' in log:
                    # log = log.replace('<MESSAGEBOX>', '')
                    # messagebox.showinfo('Information', log)

                # if '<MESSAGEBOX>' in log:
                    # log = log.replace('<MESSAGEBOX>', '')
                    # messagebox.showinfo('Information', log)

                if not hasattr(self, 'widget'):
                    print("No widget: {}".format(log))
                    return

                now = datetime.now().strftime("%Y-%m-%d %H:%M:%S: ")

                self.widget.config(state=NORMAL)
                self.widget.insert(END, now + log + '\n')
                self.widget.see(END)
                self.widget.config(state=DISABLED)

                if now:
                    self.widget.tag_add("start", self.widget.index('end-2l'), self.widget.index('end-2l+{}c'.format(len(now))))
                    self.widget.tag_config("start", foreground="#00AFEF", font=("mono", 9))

            except Empty:
                pass

            except RuntimeError:  #window closed
                return

            time.sleep(0.01)


