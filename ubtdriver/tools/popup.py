"""
===================
Custom Popup Widget
===================

TkInter not include a popup feature, this is a custom one.
"""

from tkinter import Menu
import re

########################################################################
class CustomPopup(Menu):
    """Custom popup message."""

    #----------------------------------------------------------------------
    def __init__(self, parent, text, command=None):
        """Create popup.

        Parameters
        ==========

        parent: TkInter element
            Widget that will trigger the popup message.
        text: str
            The message tha will be displayed.
        command: function, optional
            The callback funtion when a click event is generated.
        """

        self._com = command
        Menu.__init__(self,parent, tearoff=0)
        if not isinstance(text, str):
            raise TypeError('Trying to initialise a Hover Menu with a non string type: ' + text.__class__.__name__)

        toktext = re.split('\n', text)
        for t in toktext:
            self.add_command(label=t)
        self._displayed = False

        self.master.bind('<Enter>', self.__on_display__ )
        self.master.bind('<Leave>', self.__on_remove__ )


    #----------------------------------------------------------------------
    def __del__(self, *args, **kwargs):
        """Unbind events."""

        try:
            self.master.unbind('<Enter>')
            self.master.unbind('<Leave>')
        except:
            pass


    #----------------------------------------------------------------------
    def __on_display__(self, event):
        """Show popup.

        Parameters
        ----------
        event: TkInter event
            Mouse event that contains the coordinates for positioning the popup.
        """

        if not self._displayed:
            self._displayed = True
            self.post(event.x_root+5, event.y_root+5)
        if self._com != None:
            self.master.unbind_all('<Return>')
            self.master.bind_all('<Return>', self.__on_click__)


    #----------------------------------------------------------------------
    def __on_remove__(self, *args, **kwargs):
        """Destroy popup."""

        if self._displayed:
            self._displayed = False
            self.unpost()
        if self._com != None:
            self.unbind_all('<Return>')


    #----------------------------------------------------------------------
    def __on_click__(self, *args, **kwargs):
        """Callback function."""

        self._com()
