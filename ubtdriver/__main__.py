"""
==========
UBT Driver
==========
"""

from tkinter import Tk
from ubtdriver.frame import UBTDriver

#----------------------------------------------------------------------
def main():
    """Entry point for command line invocation."""

    root = Tk()
    app = UBTDriver(root)

    app.mainloop()

if __name__ == '__main__':
    main()
