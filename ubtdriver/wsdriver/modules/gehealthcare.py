"""
===========================
PyCollect Websocket Handler
===========================


Commands
========

.. include:: ../content/pycollect_commands.rst

"""

import os
import time
import json
from datetime import timedelta, datetime
from pandas import DataFrame
from scipy.signal import resample

from tornado.ioloop import IOLoop

from pycollect import GEDevice, GEDecode, measures
from pycollect.database import RAWS_ABSPATH
from ubtdriver.wsdriver.modules.base import WSHandler


########################################################################
class WSGE(WSHandler):
    """Websocket handler.

    Methods defined in this class works like commands available for the client.
    """

    NAME = 'PYCOLLECT'

    # ----------------------------------------------------------------------
    def __init__(self, *args, **kwargs):
        """Start Websocket."""

        super().__init__(*args, **kwargs)

        self.__connect_device__(self.device_config)
        self.device.on_connection_loss = self.__onconnectionloss__

    # ----------------------------------------------------------------------
    def reconnect_device(self, **request):
        """Send an instruction for reconnect with the serial port."""

        self.__connect_device__(self.device_config)
        self.debug(**request)

    # ----------------------------------------------------------------------
    @classmethod
    def __connect_device__(self, device_config, force=False):
        """Connect with the device or reuse an existing conection.

        Parameters
        ----------
        device_config: dict
            Configuration from main window.
        force: bool, optional
            This boolean will disable the option to reuse an existing conection and create new one.
        """

        if not force and hasattr(self, 'device'):
            self.logger.put('Reusing device connection')
            return

        port = device_config['port']
        debug_mode = device_config['debug_mode']

        if debug_mode:
            self.logger.put("Running in debug mode")

        if debug_mode:
            self.device = GEDevice(sorted(RAWS_ABSPATH)[0])
        else:
            self.device = GEDevice()

        connected = False
        tries = 1
        tries_t = 10

        while not connected:
            connected = self.device.connect(port, timeout=0.1)
            if not connected:
                self.logger.put("Try {}/{}: fail to connect to port {}".format(tries, tries_t, port))
                time.sleep(2)
                tries += 1

            if tries > tries_t:
                return

        if port:
            self.logger.put('Connected to port {}'.format(port))

        self.decoder = GEDecode(self.device.BUFFER)

        self.TRANSFER_DISP = False
        self.TRANSFER_WAVE = False
        self.TRANSFER_MIXTURE = False
        self.RECORDING = False
        self.RESAMPLE = 0.25
        self.BUFFER = 1

        self.device.waveforms = []
        self.device.subrecords = []

    # ----------------------------------------------------------------------
    def __check_device__(self, **request):
        """If device is not available, the stream will be closed."""

        if not self.device.READING:
            self.send_data({'type': 'LOG',
                            'message': 'NOT READING',
                            },

                           request)
            self.close_stream()
            return False
        return True

    # ----------------------------------------------------------------------
    def __restart_decoding__(self):
        """After clear buffer the decoding must be resynchronized."""

        if not self.device.READING:
            self.device.collect(True)

        self.decoder.BUFFER = self.device.BUFFER
        self.decoder.FILTER_SUBRECORDS = self.device.subrecords
        self.decoder.FILTER_WAVEFORMS = self.device.waveforms

        self.decoder.process(True)

    # ----------------------------------------------------------------------
    def request_waveforms(self, **request):
        """Request to monitor a set of waveforms.

        Parameters
        ----------
        waveforms: list
            List of waveforms labels.
        resampled: float
            Resampling factor, 1 means full data, 0.5 means resample to 50% of total data.
        """

        self.device.waveforms = request['waveforms']
        resampled = request.get('resampled', 0.25)
        buffer = request.get('buffer', 1)

        if not self.device.waveforms:
            return

        try:
            self.device.request(waveform_set=self.device.waveforms)
        except Exception as e:

            self.send_data({'type': 'ERROR',
                            'message': str(e)
                            },

                           request)
            self.stop_transfer()
            return

        self.__restart_decoding__()
        self.TRANSFER_WAVE = True
        self.RESAMPLE = resampled
        self.BUFFER = buffer
        self.__send_wave__(**request)

    # ----------------------------------------------------------------------
    def request_subrecords(self, **request):
        """Request to monitor a set of subrecords.

        Parameters
        ----------
        subrecords: list
            List of subrecords labels.
        """

        self.device.subrecords = request['subrecords']

        self.device.request(self.device.DISPL, interval=1)
        self.__restart_decoding__()
        self.TRANSFER_DISP = True
        self.__send_disp__(**request)

    # ----------------------------------------------------------------------
    def request_channels(self, **request):
        """Request to monitor a set of waveforms and subrecords.

        Parameters
        ----------
        labels: list
            List of labels, a mixture of waveforms and subrecords.
        resampled: float, optional
            Resampling factor, 1 means full data, 0.5 means resample to 50% of total data.
        """

        labels = request.get('channels', [])
        resampled = request.get('resampling', 0.25)
        buffer = request.get('buffer', 1)

        if not labels:
            labels = self.device.DEFAULT_REQUEST

        self.device.waveforms = [w['label'] for w in measures.WAVEFORMS if w['label'] in labels]
        self.device.subrecords = [label for label in labels if label not in self.device.waveforms]

        if self.device.subrecords:
            disp = self.device.DISPL
        else:
            disp = None

        if self.device.waveforms:
            waves_set = self.device.waveforms[:]
        else:
            waves_set = None

        self.device.request(subtype=disp, waveform_set=waves_set, interval=1)

        self.__restart_decoding__()
        self.TRANSFER_WAVE = False
        self.TRANSFER_DISP = False
        self.TRANSFER_MIXTURE = True
        self.RESAMPLE = resampled
        self.BUFFER = buffer
        self.__send_mixture__(**request)

    # ----------------------------------------------------------------------
    def stop_transfer(self, **request):
        """Stop the monitor data transfer.

        For consequence, the system will detect that data transmission stopped
        and stop it them too.
        """

        self.device.collect(False)
        self.decoder.process(False)

        try:
            self.device.stop()
        except:
            pass

        self.TRANSFER_DISP = False
        self.TRANSFER_WAVE = False
        self.TRANSFER_MIXTURE = False

        self.send_data({'type': 'LOG',
                        'message': 'STOP'},
                       request)

    # ----------------------------------------------------------------------
    def clear_data(self, **request):
        """Clear buffer and stop data transfer."""

        try:
            # self.device.stop()
            self.device.clear_buffer()
            self.decoder.clear_data()
            self.decoder.clear_buffer()
        except:
            pass

        self.stop_transfer()

        self.send_data({'type': 'LOG',
                        'message': 'CLEAR',
                        },

                       request)

    # ----------------------------------------------------------------------
    def start_recording(self, **request):
        """Clear data, continues the data transfer."""

        # if self.TRANSFER_DISP or self.TRANSFER_WAVE or self.TRANSFER_MIXTURE:
        if self.device.READING:

            try:
                # self.device.stop()
                self.device.clear_buffer()
                self.decoder.clear_data()
                self.decoder.clear_buffer()
            except:
                pass

            self.RECORDING = True

            self.__restart_decoding__()
            # IOLoop.instance().add_timeout(timedelta(seconds=0.5), self.__restart_decoding__)

            self.send_data({'type': 'LOG',
                            'message': 'RECORDING',
                            },
                           request)

        else:
            self.send_data({'type': 'LOG',
                            'message': 'NO DATA REQUESTED',
                            },
                           request)

    # ----------------------------------------------------------------------
    def stop_recording(self, **request):
        """"""
        if self.RECORDING:
            self.get_csv(**request)
            self.get_raw(**request)
            self.get_edf(**request)

        self.RECORDING = False

        self.send_data({'type': 'LOG',
                        'message': 'RECORDING STOPPED',
                        },
                       request)

    # ----------------------------------------------------------------------
    def __send_wave__(self, **request):
        """If data available, send JSON object each 0.125 seconds."""

        if not (self.TRANSFER_WAVE and self.__check_device__(**request)):
            return

        waves = {
            'type': 'WAVE',
            'channels': [],
        }

        for key in list(self.decoder.DATA_WAVE.keys()):
            data = self.decoder.DATA_WAVE.pop(key)
            #data_values = data[key].values.tolist()
            #data_values = data[key].values[::int(1 / self.RESAMPLE)].tolist()
            data_values = list(resample(data[key].values, int(data.shape[0] * self.RESAMPLE)))

            datetime_ = data['datetime'].values.tolist()[0] / 1e9

            waves['channels'].append({'label': key,
                                      'datetime': datetime_,
                                      'physicalDimension': measures.WAVEFORMS_DICT[key]['unit'],
                                      'samplefrequency': measures.WAVEFORMS_DICT[key]['samps'] * self.RESAMPLE,
                                      'samples': len(data_values),

                                      'physicalMaximum': measures.WAVEFORMS_DICT[key]['physical_min'],
                                      'physicalMinimum': measures.WAVEFORMS_DICT[key]['physical_max'],

                                      'digitalMaximum': 2 ** 15,
                                      'digitalMinimum': -2 ** 15,

                                      'prefilter': measures.WAVEFORMS_DICT[key]['prefilter'],
                                      'transducer': measures.WAVEFORMS_DICT[key]['transducer'],

                                      'data': data_values,
                                      })

        if waves['channels']:
            self.send_data(waves, request)

        if self.TRANSFER_WAVE:
            IOLoop.instance().add_timeout(timedelta(seconds=self.BUFFER), self.__send_wave__, **request)

    # ----------------------------------------------------------------------

    def __send_disp__(self, **request):
        """If data available, send JSON object each 0.125 seconds."""

        if not (self.TRANSFER_DISP and self.__check_device__(**request)):
            return

        if not self.decoder.DATA_SUBRECORD.empty:

            last_data = json.loads(self.decoder.DATA_SUBRECORD.iloc[-1].to_json())
            datetime_ = last_data.pop('datetime') / 1e9

            self.send_data({'type': 'DISP',
                            'data': last_data,
                            'datetime': datetime_,
                            },

                           request)

        if self.TRANSFER_DISP:
            IOLoop.instance().add_timeout(timedelta(seconds=self.BUFFER), self.__send_disp__, **request)

    # ----------------------------------------------------------------------
    def __send_mixture__(self, **request):
        """If data available, send JSON object each 0.125 seconds."""

        if not (self.TRANSFER_MIXTURE and self.__check_device__(**request)):
            return

        waves = {
            'type': 'CHANNELS',
            'channels': [],
        }

        for key in list(self.decoder.DATA_WAVE.keys()):
            data = self.decoder.DATA_WAVE.pop(key)
            #data_values = data['values'].values.tolist()
            #data_values = data['values'].values[::int(1/self.RESAMPLE)].tolist()
            data_values = list(resample(data['values'].values, int(data.shape[0] * self.RESAMPLE)))

            datetime_ = data['datetime'].values.tolist()[0] / 1e9

            waves['channels'].append({'label': key,
                                      'datetime': datetime_,
                                      'physicalDimension': measures.WAVEFORMS_DICT[key]['unit'],
                                      'samplefrequency': measures.WAVEFORMS_DICT[key]['samps'] * self.RESAMPLE,
                                      'samples': len(data_values),

                                      'physicalMaximum': measures.WAVEFORMS_DICT[key]['physical_min'],
                                      'physicalMinimum': measures.WAVEFORMS_DICT[key]['physical_max'],

                                      'digitalMaximum': 2 ** 15,
                                      'digitalMinimum': -2 ** 15,

                                      'prefilter': measures.WAVEFORMS_DICT[key]['prefilter'],
                                      'transducer': measures.WAVEFORMS_DICT[key]['transducer'],

                                      'data': data_values,
                                      })

        if not self.decoder.DATA_SUBRECORD.empty:

            temp_data_subrecord = self.decoder.DATA_SUBRECORD.copy()
            self.decoder.DATA_SUBRECORD = DataFrame()

            last_data = json.loads(temp_data_subrecord.iloc[0].to_json())
            datetime_ = last_data.pop('datetime') / 1e9

            keys = list(temp_data_subrecord.columns)

            for key in keys:
                if key == 'datetime':
                    continue

                data = temp_data_subrecord[key].tolist()
                # data = self.decoder.DATA_SUBRECORD.pop(key).tolist()

                waves['channels'].append({'label': key,
                                          'datetime': datetime_,
                                          'physicalDimension': '',
                                          'samplefrequency': '',
                                          'samples': len(data),

                                          'physicalMaximum': '',
                                          'physicalMinimum': '',

                                          'digitalMaximum': 2 ** 15,
                                          'digitalMinimum': -2 ** 15,

                                          'prefilter': '',
                                          'transducer': '',

                                          'data': data,
                                          })

        if waves['channels']:
            self.send_data(waves, request)

        if self.TRANSFER_MIXTURE:
            IOLoop.instance().add_timeout(timedelta(seconds=self.BUFFER), self.__send_mixture__, **request)

    # ----------------------------------------------------------------------
    def available_subrecords(self, **request):
        """Request the list of available subrecords based on the monitor modules activity."""

        self.device.request(self.device.DISPL, interval=-1)

        self.__restart_decoding__()

        while not self.decoder.MEANSURES_AVAILABLE:
            time.sleep(0.2)

        self.stop_transfer()

        data = {}
        data['type'] = 'RESP'
        data['subrecords'] = self.decoder.MEANSURES_AVAILABLE

        self.send_data(data, request)

    # ----------------------------------------------------------------------
    def possible_waveforms(self, **request):
        """Request the list of waveforms supported by the monitor."""

        data = {}
        data['type'] = 'RESP'
        data['waveforms'] = [w['label'] for w in measures.WAVEFORMS]

        self.send_data(data, request)

    # ----------------------------------------------------------------------
    def available_channels(self, **request):
        """Request the list of available subrecords based on the monitor modules activity merged with the list of waveforms supported by the monitor."""

        self.device.request(self.device.DISPL, interval=-1)

        self.__restart_decoding__()

        while not self.decoder.MEANSURES_AVAILABLE:
            time.sleep(0.2)

        self.stop_transfer()

        data = {}
        data['type'] = 'RESP'
        data['channels'] = self.decoder.MEANSURES_AVAILABLE + [w['label'] for w in measures.WAVEFORMS]

        self.send_data(data, request)

    # ----------------------------------------------------------------------
    def debug(self, **request):
        """Request debug information about device, conection, data transfering."""

        data = self.device_config.copy()
        data['type'] = 'RESP'
        # data.update(self.__get_status__())
        self.send_data(data, request)

    # ----------------------------------------------------------------------
    def __get_debug__(self):
        """Generate a dictionary with debug data.

        Returns
        -------
        dict
             Dictionary with debug information.
        """

        data = {}
        data['buffersize'] = len(self.decoder.BUFFER)
        data['recording'] = self.RECORDING
        data['transmitting'] = self.TRANSFER_DISP or self.TRANSFER_MIXTURE or self.TRANSFER_WAVE

        if not self.decoder.__DATA_SUBRECORD__.empty:
            data['subrecords'] = {}
            data['subrecords']['startdatetime'] = self.decoder.__DATA_SUBRECORD__.iloc[0]['datetime'].timestamp()
            data['subrecords']['enddatetime'] = self.decoder.__DATA_SUBRECORD__.iloc[-1]['datetime'].timestamp()
            data['subrecords']['totalseconds'] = (self.decoder.__DATA_SUBRECORD__.iloc[-1]['datetime'] - self.decoder.__DATA_SUBRECORD__.iloc[0]['datetime']).total_seconds()

            labels = self.decoder.__DATA_SUBRECORD__.columns.tolist()
            labels.pop(labels.index('datetime'))
            data['subrecords']['labels'] = labels

        if self.decoder.__DATA_WAVE__:
            data['waveforms'] = {}
            data['waveforms']['startdatetime'] = self.decoder.__DATA_WAVE__[list(self.decoder.__DATA_WAVE__.keys())[0]].iloc[0]['datetime'].timestamp()
            data['waveforms']['enddatetime'] = self.decoder.__DATA_WAVE__[list(self.decoder.__DATA_WAVE__.keys())[0]].iloc[-1]['datetime'].timestamp()
            data['waveforms']['totalseconds'] = (self.decoder.__DATA_WAVE__[list(self.decoder.__DATA_WAVE__.keys())[0]].iloc[-1]['datetime'] - self.decoder.__DATA_WAVE__[list(self.decoder.__DATA_WAVE__.keys())[0]].iloc[0]['datetime']).total_seconds()
            data['waveforms']['labels'] = list(self.decoder.__DATA_WAVE__.keys())

        return data

    # ----------------------------------------------------------------------
    def list_all_measures(self, **request):
        """Request a list of all measures supported by the monitor."""

        data = measures.LABEL_TO_DICT.copy()
        data['type'] = 'RESP'

        self.send_data(data, request)

    # ----------------------------------------------------------------------
    def list_all_waveforms(self, **request):
        """Request a list of all waveform supported by the monitor."""
        data = measures.WAVEFORMS_DICT.copy()
        data['type'] = 'RESP'

        self.send_data(data, request)

    # ----------------------------------------------------------------------
    def get_csv(self, **request):
        """Generate and save data un CSV format."""

        start_datetime = self.__get_first_datetime__()
        if start_datetime is None:
            return

        parent_dir = str(datetime.fromtimestamp(start_datetime)).replace(':', '-')

        filename = os.path.join(self.device_config['user_dir'], parent_dir, '{}.csv'.format(start_datetime))
        static = self.device_config['download']

        if not os.path.exists(os.path.dirname(filename)):
            # os.mkdir(os.path.dirname(filename))
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        filenames = self.decoder.save_as_csv(filename)
        for filename in filenames:
            self.logger.put('CSV generated in: {}'.format(filename))

        dirname = os.path.split(os.path.dirname(filenames[0]))[-1].replace(' ', '%20')
        filenames = [os.path.split(f)[-1] for f in filenames]

        data = {'filenames': ['{}{}/{}'.format(static, dirname, f) for f in filenames], 'type': 'RESP', }
        self.send_data(data, request)

        self.RECORDING = False

    # ----------------------------------------------------------------------
    def get_raw(self, **request):
        """Generate and save data in RAW format."""

        start_datetime = self.__get_first_datetime__()
        if start_datetime is None:
            return

        parent_dir = str(datetime.fromtimestamp(start_datetime)).replace(':', '-')

        filename = os.path.join(self.device_config['user_dir'], parent_dir, '{}.raw'.format(start_datetime))
        static = self.device_config['download']

        if not os.path.exists(os.path.dirname(filename)):
            # os.mkdir(os.path.dirname(filename))
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        filenames = self.decoder.save_as_raw(filename)
        for filename in filenames:
            self.logger.put('RAW generated in: {}'.format(filename))

        dirname = os.path.split(os.path.dirname(filenames[0]))[-1].replace(' ', '%20')
        filenames = [os.path.split(f)[-1] for f in filenames]

        data = {'filenames': ['{}{}/{}'.format(static, dirname, f) for f in filenames], 'type': 'RESP', }
        self.send_data(data, request)

        self.RECORDING = False

    # ----------------------------------------------------------------------
    def get_edf(self, **request):
        """Generate and save data in EDF+ format.

        Parameters
        ----------
        patientInfo: dict, optional
            Dictionary with the EDF header.

        annotations: dict, optional
            Dictionary with the annotations.

             Example:

            .. code::

                {"size": 1,
                  "items": [
                    {
                      "onset": 779.94,
                      "duration": 0.004,
                      "description": "OJOS_CERRADOS"
                    },
                  ]
                }
        """

        start_datetime = self.__get_first_datetime__()
        if start_datetime is None:
            return

        parent_dir = str(datetime.fromtimestamp(start_datetime)).replace(':', '-')

        filename = os.path.join(self.device_config['user_dir'], parent_dir, '{}.edf'.format(start_datetime))
        static = self.device_config['download']

        header = request.get('patientInfo', {})
        annotations = request.get('annotations', {'items': [], })

        edf_header = {
            "admincode": header.get('adminCode', ''),
            "birthdate": header.get('birthDate', ''),
            "equipment": header.get('equipment', ''),
            "gender": header.get('gender', ''),
            "patientode": header.get('patientCode', ''),
            "patientname": header.get('patientName', ''),
            "patient_additional": header.get('patientAdditional', ''),
            "recording_additional": header.get('recordingAdditional', ''),
            "technician": header.get('technician', ''),

            "startdate": start_datetime,
        }

        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        filenames = self.decoder.save_as_edf(filename, edf_header, annotations['items'])

        for filename in filenames:
            self.logger.put('EDF generated in: {}'.format(filename))

        dirname = os.path.split(os.path.dirname(filenames[0]))[-1].replace(' ', '%20')
        filenames = [os.path.split(f)[-1] for f in filenames]

        data = {'filenames': ['{}{}/{}'.format(static, dirname, f) for f in filenames], 'type': 'RESP', }
        self.send_data(data, request)

        self.RECORDING = False

    # ----------------------------------------------------------------------
    def __get_first_datetime__(self):
        """Parse the datetime for the first element in ``subrecords`` or ``waveforms``.

        Returns
        -------
        timestamp
            The lower datetime from both types of signals.
        """

        status = self.__get_debug__()

        t = []
        if 'subrecords' in status:
            t.append(status['subrecords']['startdatetime'])

        if 'waveforms' in status:
            t.append(status['waveforms']['startdatetime'])

        if not t:
            return 0

        return min(t)

    # ----------------------------------------------------------------------
    def close_stream(self, **request):
        """Stop stream and close serial port, save data if a recording enabled."""

        try:
            self.stop_transfer()
        except:
            pass

        if self.RECORDING:
            self.get_csv(**request)
            self.get_raw(**request)
            self.get_edf(**request)

        self.send_data({'type': 'LOG',
                        'message': 'STREAM CLOSED',
                        },
                       request)

        self.device.device.close()
        self.close()

    # ----------------------------------------------------------------------
    def __onconnectionloss__(self):
        """When connection lost the system will try to reconnect automatically."""

        self.logger.put("Connection with serial device lost.")

        self.logger.put("Tring to reconnect...")
        self.__connect_device__(self.device_config, force=True)


if __name__ == '__main__':

    try:
        rest = open('../../../docs/source/content/pycollect_commands.rst', 'w')
    except:
        print("You should not be running this file.")
        import sys
        sys.exit()

    global_ = [func for func in dir(WSGE) if callable(getattr(WSGE, func)) if not func.startswith('_')]
    heritage = [func for func in dir(WSHandler) if callable(getattr(WSHandler, func)) if not func.startswith('_')]

    commands = [h for h in global_ if h not in heritage]

    for command in commands:
        rest.write('  * ``{}``: {}\n'.format(command, getattr(WSGE, command).__doc__.split('\n')[0]))

    rest.close()
