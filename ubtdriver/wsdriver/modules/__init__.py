"""
=======
modules
=======

Modules are Websockets templates that enable the data transfering with the
protocol format defined in :ref:`WSHandler`.
"""

from .gehealthcare import WSGE
from .masimo import WSMasimo