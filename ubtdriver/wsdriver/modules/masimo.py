"""
==========================
PyMasimo Websocket Handler
==========================

"""

from ubtdriver.wsdriver.modules.base import WSHandler


########################################################################
class WSMasimo(WSHandler):
    """Template for WSMasimo handler."""
    NAME = "PYMASIMO"

