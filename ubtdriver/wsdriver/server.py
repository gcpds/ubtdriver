"""
===========
HTTP Server
===========

Tornado HTTP server with a WebSocket running in it, it is composed of:

  * **Application**: A collection of request handlers that make up a web application.
  * **HTTPServer**: A non-blocking, single-threaded HTTP server.
"""

import threading

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application, url

from .modules import WSGE, WSMasimo

DEBUG = True

#----------------------------------------------------------------------
def make_app(config, logger):
    """A collection of request handlers that make up a web application.

    Parameters
    ----------
    logger: signal
        Signal for write messages in asynchronous way.
    """

    settings = {
        'debug': DEBUG,
        'static_path': config['user_dir'],
        'static_url_prefix': '/download/',  #The static file handler is used for access/download generated files.
        'xsrf_cookies': False,
    }

    WS = {
        'gehealthcare': WSGE,
        'masimo': WSMasimo,

    }[config['device']]

    if logger:
        WSGE.logger = logger
        WSMasimo.logger = logger

    config['download'] = 'http://localhost:{}{}'.format(config['ip_port'], settings['static_url_prefix'])

    return Application([

        url(r'^/ws', WS, {'device_config': config,}),

    ], **settings)



#----------------------------------------------------------------------
def run_websocket(config, logger=None):
    """Entry point for invote the Websocket from main window.

    Parameters
    ----------
    config: dict
        Object with configurations from main window.
    logger: signal
        Signal for write messages in asynchronous way.

    Returns
    -------
    HTTPServer
        Server object that can be stoped in the main window.
    """

    port = int(config['ip_port'])

    logger.put("UBT-launcher running on port {}".format(port))
    application = make_app(config, logger)
    # asyncio.set_event_loop(asyncio.new_event_loop())
    http_server = HTTPServer(application)

    http_server.listen(port)
    # IOLoop.instance().start()

    if hasattr(IOLoop.instance(), 'asyncio_loop'):

        # Start new IOLoop only if previous is dead or not exist
        if not IOLoop.instance().asyncio_loop.is_running():
            try:
                # Using a tthread for keep main window alive
                # IOLoop.instance().start()
                threading.Thread(target=IOLoop.instance().start, args=()).start()
            except:
                pass  #TODO: Windows test

    else:  #Sometimes this instance ('asyncio_loop') not exist in windows.
        threading.Thread(target=IOLoop.instance().start, args=()).start()

    return http_server  #returning server to main window


#----------------------------------------------------------------------
def stop_websocket():
    """Try to stop server.

    `Try` means that the task will be released to OS.
    """

    ioloop = IOLoop.current()
    ioloop.add_callback(ioloop.stop)


if __name__ == '__main__':
    run_websocket()
